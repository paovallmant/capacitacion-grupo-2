import { Injectable } from '@nestjs/common';
import { USUARIOS } from './constantes/usuarios';

@Injectable()
export class UsuarioService {
    usuarios = USUARIOS;
    listarUsuarios(){
        return this.usuarios;
    }

    filtrarPorId(idParametro: string){
        const usuarioFiltrado = this.usuarios.filter((usuario) => usuario._id === idParametro);
        // si no encuentra el usuario retorna arreglo vacio
        return usuarioFiltrado;
    }

    crearUsuario(usuarioACrear){
        this.usuarios.push(usuarioACrear);
        return usuarioACrear;
    }

    actualizarUsuario(idParametro: string, dataActualizar){
        const usuarioFiltrado = this.filtrarPorId(idParametro);
        if(usuarioFiltrado.length > 0) {
            const indice = this.usuarios.indexOf(usuarioFiltrado[0]);
            this.usuarios[indice] = dataActualizar;
            return this.usuarios[indice];
        }
        else{
            return false;
        }
    }

    eliminarUsuario(idParametro: string){
        const usuarioFiltrado = this.filtrarPorId(idParametro);
        if(usuarioFiltrado.length > 0) {
            const indice = this.usuarios.indexOf(usuarioFiltrado[0]);
            const usuarioEliminado = this.usuarios.splice(indice, 1);
            return true;
        }
        else{
            return false;
        }
        // si no encuentra el usuario retorna arreglo vacio
        return usuarioFiltrado;
    }

}

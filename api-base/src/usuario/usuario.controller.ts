import { UsuarioService } from './usuario.service';
import { Body, Controller, Delete, Get, Param, Post, Put, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Controller('usuario') //path principal http://localhost:3000/usuario
export class UsuarioController {

    constructor(
        private readonly usuarioService : UsuarioService
    ){}

    @UseGuards(AuthGuard('jwt')) // con este decorador se solicita token para proteger el endpoint
    @Get('obtener-usuarios') //path principal http://localhost:3000/usuario/obtener-usuarios
    obtenerUsuarios(@Request() request){
        console.log('request', request.user);
        const usuarios = this.usuarioService.listarUsuarios();
        const {email} = request.user;
        return {
            mensaje: 'Todos los usuarios',
            data: usuarios,
            email
        }
    }

    @Delete('eliminar-usuario/:id') //http://localhost:3000/usuario/eliminar-usuario
    eliminarUsuario(@Param('id') idParametro: string){
        const usuarioEliminado = this.usuarioService.eliminarUsuario(idParametro);
        return {
            mensaje: 'Usuario eliminado',
            data: usuarioEliminado
        }
    }

    @Post('crear-usuario') //http://localhost:3000/usuario/crear-usuario
    crearUsuario(@Body() usuarioACrear){
        /*const usuarioACrear = {
            nombre: 'test'
        }*/
        const usuarioCreado = this.usuarioService.crearUsuario(usuarioACrear);
        return {
            mensaje: 'usuario creado',
            data: usuarioCreado
        }

    }

    @Put('editar-usuario/:id') //http://localhost:3000/usuario/editar-usuario
    editarUnUsuario(@Param('id') idParametro: string,
    @Body() usuarioAActualizar){
        const usuarioActualizado = this.usuarioService.actualizarUsuario(idParametro, usuarioAActualizar)
        return {
            mensaje: 'usuario editado',
            data:usuarioActualizado
        }
    }

//    @UseGuards(AuthGuard('jwt')) // con este decorador se solicita token para proteger el endpoint
    @Get('obtener-por-id/:id') //http://localhost:3000/usuario/obtener-por-id/:id
    filtrarProId(@Param('id') id: string)
    {
        const usuarioFiltrado = this.usuarioService.filtrarPorId(id)
        return {
            mensaje: 'usuario filtrado por id',
            data: usuarioFiltrado
        }
    }

    
}

import { JwtService } from '@nestjs/jwt';
import { CredencialesInterface } from './credenciales.interface';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AuthService {
    constructor(private readonly jwtService: JwtService){}
    loginConCredenciales(credenciales: CredencialesInterface){

        const payload = {
            email: credenciales.usuario,
            clave: credenciales.password
        };

        const token = this.jwtService.sign(payload);
        return token
    }
}

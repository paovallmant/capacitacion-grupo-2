import { configuracion } from './../configuracion/configuracion';
import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy, StrategyOptions } from "passport-jwt";

@Injectable() 
export class JwtStrategy  extends PassportStrategy(Strategy){

    constructor(){
        const configuracionJWT : StrategyOptions = { 
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: configuracion.auth0.secreto
        }
        super(configuracionJWT)
    }

    validate(payload){
        return payload
    }

}
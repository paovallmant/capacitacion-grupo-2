import { AuthService } from '@auth0/auth0-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'auth0-cliente';

  constructor(private readonly authService: AuthService){

  }

login(){
  //alert('login');
  this.authService.loginWithRedirect();
}

logout(){
  //alert('logout');
  this.authService.logout();
};

}

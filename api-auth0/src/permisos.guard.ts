import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

@Injectable()
export class PermisosGuard implements CanActivate {

constructor(private reflector: Reflector){}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const permisosRuta = this.reflector.get<string[]>('permissions', context.getHandler())

    if(!permisosRuta){
      return true;
    }

    const permisosUsuario = context.getArgs()[0].user.permissions;
    permisosUsuario.push('listar-usuarios')


    const tienePermisos = () => permisosRuta.every(
      rutaPermiso => permisosUsuario.includes(rutaPermiso)
    )

    return tienePermisos();

  }
}

import { USUARIOS } from './constantes/usuarios';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UsuarioService {

    usuarios = USUARIOS;

    listarUsuarios(){
        return this.usuarios;
    }

    filtrarPorId(idParametro: string){
        const usuarioFiltrado = this.usuarios
        .filter((usuario: any) => 
        usuario._id === idParametro);
        // si no encuentra el usuario 
        // va entregar un arreglo vacio
        return usuarioFiltrado;
    }

    crearUsuario(usuarioACrear){
        this.usuarios.push(usuarioACrear);
        return usuarioACrear
    }

    eliminarUsuario(idParametro: string){
        const usuarioFiltrado = this.filtrarPorId(idParametro);
        if(usuarioFiltrado.length > 0){
            const indice = this.usuarios.indexOf(usuarioFiltrado[0]);
            const usuarioEliminado = this.usuarios.splice(indice,1)
            return true;
        }else {
            return false;
        }
    }

    actualizar(idParametro: string, dataAActualizar) {
        const usuarioFiltrado = this.filtrarPorId(idParametro);
        if(usuarioFiltrado.length > 0){
            const indice = this.usuarios.indexOf(usuarioFiltrado[0]);
            this.usuarios[indice] = dataAActualizar
            return this.usuarios[indice];
        }else {
            return false;
        }
    }
}

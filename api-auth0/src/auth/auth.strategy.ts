import { ExtractJwt } from 'passport-jwt';
import { StrategyOptions } from 'passport-jwt';
import { Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { passportJwtSecret } from 'jwks-rsa';
import * as dotenv from 'dotenv'

dotenv.config();

export class AuthStrategy extends PassportStrategy(Strategy){
    
    constructor(){
        const configuracionStrategy: StrategyOptions = {
            secretOrKeyProvider: passportJwtSecret({
                cache: true,
                rateLimit: true,
                jwksRequestsPerMinute: 5,
                jwksUri: `${process.env.AUTH0_DOMINIO}.well-known/jwks.json`
                //jwksUri: process.env.AUTH0_DOMINIO + '.well-known/jwks.json'
            }),
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            audience: 'http://ejemplo.com', //clientId
            issuer: process.env.AUTH0_DOMINIO,  //dominio
            algorithm: ['RS256'],
        }

        super(
            configuracionStrategy
        );
    }

    validate(payload:any){
        return payload;
    }
}
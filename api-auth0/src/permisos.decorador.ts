import { SetMetadata } from "@nestjs/common";

export const PermisosDecorador = (...permissions: string[])=> SetMetadata('permissions', permissions)